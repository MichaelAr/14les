class Book

  attr_reader :last_person, :show_hash

  def initialize
    @hh = {}
    @last_person = ''
  end

  def add_person(options)
    # add to hash
    @last_person = options[:name]
    puts 'Already exists!' if @hh[options[:name]]

    @hh[options[:name]] = options[:age]
  end

  def show_all
    # showHash
    @hh.each_key do |key|
      age = @hh[key]
      puts "Name: #{key}, age: #{age}"
    end
  end
end

b = Book.new
b.add_person name: 'Walter', age: 50
b.add_person name: 'Mike', age: 33
b.add_person name: 'Jessie', age: 25
b.add_person name: 'Badger', age: 66
b.show_all
puts "Last person: #{b.last_person}"
puts b.show_hash
