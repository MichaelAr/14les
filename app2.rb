# to build planes
class Airplane
  attr_reader :model, :altitude, :speed

  def initialize(model)
    @model = model
    @speed = 0
    @altitude = 0
  end

  def fly
    @speed = rand(500..800)
    @altitude = rand(5_000..10_000)
  end

  def land
    @speed = 0
    @altitude = 0
  end

  def moving?
    @speed.positive?
  end
end


models = %w[Airbus-320 Boeing-777 IL-86]
planes = []

1000.times do
  model = models[rand(0..2)]
  plane = Airplane.new(model)

  if rand(0..1) == 1
    plane.fly
  end
  planes << plane
end

planes.each do |plane|
  puts "Model: #{plane.model}, Speed: #{plane.speed}, Altitude: #{plane.altitude}"
  puts "Is moving: #{plane.moving?}"
end

